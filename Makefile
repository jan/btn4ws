# Makefile for btn4ws.py
#
# copyright (c) 1999-2009 Jan Dittberner <jan@dittberner.info>
#
# This file is part of btn4ws.
#
# btn4ws is free software; you can redistribute it and/or modify it
# under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# btn4ws is distributed in the hope that it will be useful, but
# WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the GNU
# General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with btn4ws; if not, write to the Free Software Foundation,
# Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
#
# version: $Id$
#

GIMPTOOL = gimptool-2.0

install:
	install -d $(PREFIX)/$$($(GIMPTOOL) --gimpplugindir)/plug-ins
	install btn4ws.py $(PREFIX)/$$($(GIMPTOOL) --gimpplugindir)/plug-ins

install-user:
	$(GIMPTOOL) --install-bin btn4ws.py

clean:
	find . -type f -name '*~' -exec rm {} \;

dist:
	python setup.py sdist

.PHONY: install install-user clean dist
