#!/usr/bin/env python
# -*- coding: utf-8 -*-
# This is the distutils setup file for btn4ws.
#
# Copyright (c) 2007-2009 Jan Dittberner <jan@dittberner.info>
#
# This file is part of btn4ws.
#
# btn4ws is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# btn4ws is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with btn4ws; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
# version: $Id$
#

from distutils.core import setup

setup(name='btn4ws',
      version='0.8.0.1',
      description='Buttons for websites GIMP plugin',
      author='Jan Dittberner',
      author_email='jan@dittberner.info',
      url='http://www.dittberner.info/project/btn4ws/',
      py_modules=['btn4ws']
      )
